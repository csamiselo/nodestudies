# NodeStudies

Mini Projetos de Cursos, Livros e artigos de Node.js.

Aqui tenho projetos desde Node.js puro focado em padrões, tecnicas avançadas ou simples


## Node.js: Iniciando da teoria à prática | Masterclass #11
Pasta: node-masterclass

Link: https://www.youtube.com/watch?v=DiXbJL3iWVs

Sobre: Armazenador de URLs para testar chamadas de API

## Node.js: API Rest com Express e MongoDB
Pasta: nodejs-api-rest-alura

Link: https://cursos.alura.com.br/course/nodejs-api-rest-express-mongodb

Sobre: O objetivo vai ser construir uma API para Livraria.

## Ambiente de desenvolvimento NodeJS com Docker e Docker Compose
Pasta: docker-and-nodejs

Link: https://www.youtube.com/watch?v=AVNADGzXrrQ

Sobre: Tutorial de Docker e Docker Compose pra aplicações Node.js

## COMEÇANDO COM NODE.JS EM 2022
Pasta: iniciando-com-node

Link: https://youtu.be/fm4_EuCsQwg

Sobre:
